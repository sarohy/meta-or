//
//  CallBack.swift
//  Meta Or
//
//  Created by Sarohy on 30/10/2018.
//  Copyright © 2018 Sarohy. All rights reserved.
//

import Foundation
protocol Callback {
    func onSuccess(_ result:AnyObject)
    func onFail(_ result:AnyObject);
    func onFetchSever(_ result:AnyObject);
    func onSuccessInfo(_ result:AnyObject);
    func onSuccessHistory(_ result:AnyObject);
}
