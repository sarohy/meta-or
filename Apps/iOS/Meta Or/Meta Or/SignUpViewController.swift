//
//  SignUpViewController.swift
//  Meta Or
//
//  Created by Sarohy on 30/10/2018.
//  Copyright © 2018 Sarohy. All rights reserved.
//

import UIKit
import Network

class SignUpViewController: UIViewController, Callback {
    func onSuccess(_ result: AnyObject) {
        let msg:String = (result as AnyObject).value(forKey: "desc") as! String
        Toast.show(message: msg,controller: self)
    }
    
    func onFail(_ result: AnyObject) {
        let msg:String = (result as AnyObject).value(forKey: "desc") as! String
        print(msg)
        Toast.show(message: msg,controller: self)
    }
    
    func onFetchSever(_ result: AnyObject) {
        
    }
    
    func onSuccessInfo(_ result: AnyObject) {
        
    }
    
    func onSuccessHistory(_ result: AnyObject) {
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func signUpButtonClick(_ sender: Any) {
        NetworkManger.signup((email?.text)!,(firstName?.text!)! ,(lastName?.text!)!, self)
    }
    
    @IBOutlet var email: UITextField!
    @IBOutlet var lastName: UITextField!
    @IBOutlet var firstName: UITextField!

}
