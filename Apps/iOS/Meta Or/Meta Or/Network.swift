//
//  Network.swift
//  Meta Or
//
//  Created by Sarohy on 17/10/2018.
//  Copyright © 2018 Sarohy. All rights reserved.
//

import Foundation
import UIKit

class NetworkManger {
    init() {
    }
    public static func login(_ email:String, _ accounNumber:String,
                             _ password:String, _ serverName:String){
        let url = URL(string: Constants.URL)!
        var request = URLRequest(url: url)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        var postString = ""
        let action = "login"
        let model = UIDevice.current.model
        let systemVersion = UIDevice.current.systemVersion
        let deviceInfo = model + "-" + systemVersion
        let deviceId = DataStorage().getDeviceId()
        let date = Int(Date().timeIntervalSince1970)
        postString.append("action="+action+"&")
        postString.append("email="+email+"&")
        postString.append("mt4_account="+accounNumber+"&")
        postString.append("mt4_password="+password+"&")
        postString.append("mt4_server="+serverName+"&")
        postString.append("device_token="+deviceId+"&")
        postString.append("device_info="+deviceInfo+"&")
        postString.append("timestamp="+String(date)+"&")
        let signature = Utils.MD5(string: action+email+accounNumber+password+serverName+deviceId+deviceInfo+String(date)+Constants.KEY)
        postString.append("signature="+signature.map { String(format: "%02hhx", $0) }.joined())
        print(postString)
        print(signature)
        request.httpBody = postString.data(using: .utf8)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }

            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(String(describing: response))")
            }

            _ = String(data: data, encoding: .utf8)
            let jsonObj = try? JSONSerialization.jsonObject(with: data, options: .allowFragments)
            print("responseString = \(String(describing: (jsonObj! as AnyObject).value(forKey: "code")))")
        }
        task.resume()
    }
    public static func signup(_ email:String, _ firstName:String,
                              _ lastName:String, _ callback:Callback){
        let url = URL(string: Constants.URL)!
        var request = URLRequest(url: url)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        var postString = ""
        let action = "signup"
        let model = UIDevice.current.model
        let systemVersion = UIDevice.current.systemVersion
        let deviceInfo = model + "-" + systemVersion
        let deviceId = DataStorage().getDeviceId()
        let date = Int(Date().timeIntervalSince1970)
        postString.append("action="+action+"&")
        postString.append("email="+email+"&")
        postString.append("first_name="+firstName+"&")
        postString.append("last_name="+lastName+"&")
        postString.append("device_token="+deviceId+"&")
        postString.append("device_info="+deviceInfo+"&")
        postString.append("timestamp="+String(date)+"&")
        let signature = Utils.MD5(string: action+email+firstName+lastName+deviceId+deviceInfo+String(date)+Constants.KEY)
        postString.append("signature="+signature.map { String(format: "%02hhx", $0) }.joined())
        print(postString)
        print(signature)
        request.httpBody = postString.data(using: .utf8)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(String(describing: response))")
            }
            
            _ = String(data: data, encoding: .utf8)
            let jsonObj = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as! Dictionary<String, AnyObject>
             print(type(of: jsonObj))
            print(type(of: jsonObj?["response"]))
            if (jsonObj?["code"] as! Int == 0){
                DataStorage().setDeviceId(deviceId: deviceId)
                callback.onSuccess((jsonObj! as AnyObject).value(forKey: "response") as AnyObject)
            }
            else {
                let res = jsonObj?["response"] as! String
                let jsonResObj = try? JSONSerialization.jsonObject(with: res, options: .mutableContainers) as! Dictionary<String, String>
                print(jsonResObj)
                callback.onFail(jsonResObj as AnyObject)
            }
        }
        task.resume()
    }
}


