//
//  DataStorage.swift
//  Meta Or
//
//  Created by Sarohy on 17/10/2018.
//  Copyright © 2018 Sarohy. All rights reserved.
//

import Foundation
struct DataStorage {
    init() {
        
    }
    let defaults = UserDefaults.standard
    public func setDeviceId(deviceId:String){
        defaults.set(deviceId, forKey: "deviceId")
    }
    public func getDeviceId()-> String{
        if let stringOne = defaults.string(forKey: "deviceId" ) {
            return stringOne // Some String Value
        }
        else {
            return UUID().uuidString
        }
    }
}
