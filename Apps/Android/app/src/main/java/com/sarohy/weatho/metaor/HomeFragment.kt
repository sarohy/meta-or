package com.sarohy.weatho.metaor

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import kotlinx.android.synthetic.main.fragment_home.*
import org.json.JSONArray
import org.json.JSONObject
import android.view.MotionEvent
import android.support.v7.widget.RecyclerView
import android.support.v4.view.ViewCompat.canScrollHorizontally
import java.util.*


class HomeFragment : Fragment(){

    lateinit var list: ArrayList<ActiveTrade>
    lateinit var mAdapter: ActiveTradeAdapter
    val handler = Handler()
    var c: Context? = context



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        context?.let {
            tv_account_info.text = "["+PrefManager.getInstance(it).getAccNo() + " " + PrefManager.getInstance(it).getServerName()+"]"
        }
    }

    fun init() {
        list = ArrayList()
        mAdapter = ActiveTradeAdapter(context!!,list)
        val mLayoutManager = LinearLayoutManager(context)
        rv_trade.layoutManager = mLayoutManager
        rv_trade.itemAnimator = DefaultItemAnimator()
        rv_trade.addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
        rv_trade.adapter = mAdapter
        //rv_trade.addOnItemTouchListener(ScrollingTouchInterceptor())
        rv_trade.setNestedScrollingEnabled(false);
    }

    fun getDataWithDelay() {
        handler.postDelayed({
            fetchData()
        }, 1000)
    }

    fun fetchData() {
        context?.let { NetworkManager.getInfo(it, ProgressBar(it)) }
    }

    fun onSuccess(result: JSONObject?) {
        result?.let {
            Log.d("MD5", "Test" + result.toString())
            val response:JSONObject = it.get("result") as JSONObject
            accountUpdate(response)
            profitUpdate(response)
            updateTradeList(response)
        }
        c?.let {
            getDataWithDelay()
        }
    }

    fun profitUpdate(response: JSONObject) {
        val profit: JSONObject = response.get("profits") as JSONObject
        tv_today.text = profit.get("today") as CharSequence?
        tv_week.text = profit.get("this_week") as CharSequence?
        tv_month.text = profit.get("this_month") as CharSequence?
        tv_inception.text = profit.get("since_inception") as CharSequence?
        Utils.UpdateUIColor(context, tv_today.text as String?,tv_today)
        Utils.UpdateUIColor(context, tv_week.text as String?,tv_week)
        Utils.UpdateUIColor(context, tv_month.text as String?,tv_month)
        Utils.UpdateUIColor(context, tv_inception.text as String?,tv_inception)
    }

    fun accountUpdate(response: JSONObject) {
        val account: JSONObject = response.get("account") as JSONObject
        tv_equity.text = account.get("equity") as CharSequence?
        tv_balance.text = account.get("balance") as CharSequence?
        tv_margin.text = account.get("free_margin") as CharSequence?
        tv_floating_profit.text = account.get("floating_profit") as CharSequence?
        Utils.UpdateUIColor(context, tv_floating_profit.text as String?,tv_floating_profit)
    }
    fun updateTradeList(response: JSONObject) {
        reset()
        list.clear()
        val trades: JSONArray = response.get("trades") as JSONArray
        for (i in 0 until trades.length()) {
            val t = trades.getJSONObject(i)
            val trade = ActiveTrade(t.get("symbol") as String?, t.get("direction") as String?,
                    t.get("entry_price") as String?, t.get("size") as String?,
                    t.get("current_price") as String?,t.get("tp") as String?,t.get("sl") as String?, t.get("profit") as String?)
            list.add(trade)
        }
        if(trades.length()>0) {
            mAdapter.notifyDataSetChanged()
        }
        else {
            rv_trade.visibility = View.GONE
            rl_empty.visibility = View.VISIBLE
        }
    }
    fun reset(){
        rv_trade.visibility = View.VISIBLE
        rl_empty.visibility = View.GONE
    }

    inner class ScrollingTouchInterceptor : RecyclerView.OnItemTouchListener {
        override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
            return false
        }

        override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {}

        override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {}
    }

    override fun onStop() {
        c = null
        handler.removeCallbacksAndMessages(null)
        super.onStop()
    }

    override fun onResume() {
        c = context
        fetchData()
        super.onResume()
    }

}
