package com.sarohy.weatho.metaor

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import kotlinx.android.synthetic.main.item_row_active_trade.view.*


class ActiveTradeAdapter(c: Context, trades: ArrayList<ActiveTrade>) : RecyclerView.Adapter<ActiveTradeAdapter.MyViewHolder>() {
    private var  tradeList: ArrayList<ActiveTrade> = trades
    private var  context: Context = c

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var symbol: TextView = view.tv_symbol
        var direction: TextView = view.tv_direction_size
        var entry_price: TextView = view.tv_entry_price
        var currentPrice: TextView = view.tv_current_price
        var profit: TextView = view.tv_profit
        val sl: TextView = view.tv_sl
        val tp: TextView = view.tv_tp_sl
        var iv_flag1: ImageView = view.iv_flag1
        var iv_flag2: ImageView = view.iv_flag2
    }



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_row_active_trade, parent, false)

        return MyViewHolder(itemView)
    }

    @SuppressLint("ResourceAsColor")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val trade = tradeList[position]
        holder.currentPrice.text = trade.current_price
        holder.symbol.text = trade.symbol
        holder.entry_price.text = trade.entry_price
        holder.direction.text = trade.direction +"  "+trade.size
        holder.profit.text = trade.profit
        holder.sl.text = "SP: "+ trade.sl
        holder.tp.text = "TP: "+ trade.tp+"\nSP: "+ trade.sl
        Utils.UpdateUIColor(context,trade.profit, holder.profit)
        loadFlags(holder,trade)
    }

    private fun loadFlags(holder: MyViewHolder, trade: ActiveTrade) {
        var context = holder.iv_flag1.getContext()
        var id = context.getResources().getIdentifier(trade.symbol.substring(0, 3).toLowerCase(), "drawable", context.getPackageName())
        holder.iv_flag1.setImageResource(id)
        context = holder.iv_flag2.getContext()
        id = context.getResources().getIdentifier(trade.symbol.substring(3, 6).toLowerCase(), "drawable", context.getPackageName())
        holder.iv_flag2.setImageResource(id)
    }

    override fun getItemCount(): Int {
        return tradeList.size
    }
}