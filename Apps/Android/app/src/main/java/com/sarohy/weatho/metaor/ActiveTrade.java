package com.sarohy.weatho.metaor;

public class ActiveTrade {
    String symbol;
    String direction;
    String entry_price;
    String size;
    String current_price;
    String tp;
    String sl;
    String profit;

    public ActiveTrade(String symbol, String direction, String entry_price, String size, String current_price, String tp, String sl, String profit) {
        this.symbol = symbol;
        this.direction = direction;
        this.entry_price = entry_price;
        this.size = size;
        this.current_price = current_price;
        this.tp = tp;
        this.sl = sl;
        this.profit = profit;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getEntry_price() {
        return entry_price;
    }

    public void setEntry_price(String entry_price) {
        this.entry_price = entry_price;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getCurrent_price() {
        return current_price;
    }

    public void setCurrent_price(String current_price) {
        this.current_price = current_price;
    }

    public String getTp() {
        return tp;
    }

    public void setTp(String tp) {
        this.tp = tp;
    }

    public String getSl() {
        return sl;
    }

    public void setSl(String sl) {
        this.sl = sl;
    }

    public String getProfit() {
        return profit;
    }

    public void setProfit(String profit) {
        this.profit = profit;
    }
}
