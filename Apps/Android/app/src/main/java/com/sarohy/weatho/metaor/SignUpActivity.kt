package com.sarohy.weatho.metaor

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.View
import android.view.WindowManager
import android.widget.ProgressBar
import kotlinx.android.synthetic.main.activity_sign_up.*
import android.view.inputmethod.EditorInfo
import android.util.Patterns
import android.view.inputmethod.InputMethodManager
import org.json.JSONObject


class SignUpActivity : AppCompatActivity(), View.OnClickListener, CallBack {
    override fun onSuccess(result: JSONObject?) {
        result?.let {
            loader.visibility = View.GONE
            btn_signup.text = "Registered"
            Snackbar.make(rootLayout, it.get("desc").toString(), Snackbar.LENGTH_INDEFINITE)
                    .setAction("Ok") {
                        this.finish()
                    }
                    .setActionTextColor(resources.getColor(android.R.color.holo_red_light))
                    .show()
        }
    }

    override fun onFail(result: JSONObject?) {
        result?.let {
            loader.visibility = View.GONE
            btn_signup.text = "Register"
            Snackbar.make(rootLayout, it.get("desc").toString(), Snackbar.LENGTH_INDEFINITE)
                    .setAction("Ok") {
                        btn_signup.isClickable = true
                    }
                    .setActionTextColor(resources.getColor(android.R.color.holo_red_light))
                    .show()
        }
    }

    override fun onFetchSever(result: JSONObject?) {

    }

    override fun onSuccessInfo(result: JSONObject?) {

    }

    override fun onSuccessHistory(result: JSONObject?) {

    }

    override fun onClick(v: View?) {
        when(v?.id) {
            R.id.btn_signup -> {
                hideKeyboardFrom(this,v)
                if (Utils.isValidEmail(et_email.text) and (et_last_name.text.length>=2) and (et_last_name.text.length>=2)) {
                    this.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                    val email = et_email.text.toString();
                    val firstName = et_first_name.text.toString();
                    val lasttName = et_last_name.text.toString();
                    loader.visibility = View.VISIBLE
                    btn_signup.text = "Loading"
                    btn_signup.isClickable = false
                    NetworkManager.signUp(this, email, firstName, lasttName, ProgressBar(this))
                }
                else {
                    Utils.showToast(this,"Input is invalid")
                }
            }
            R.id.tv_login -> this.finish()
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        this.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        btn_signup.setOnClickListener(this)
        tv_login.setOnClickListener(this)
        et_last_name.setOnEditorActionListener() { v, actionId, event ->
            if(actionId == EditorInfo.IME_ACTION_DONE){
                btn_signup.performClick()
                true
            } else {
                false
            }
        }
        et_email.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (Utils.isValidEmail(et_email.text))
                    et_email.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_done, 0);
                else
                    et_email.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_close, 0);
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })
        et_first_name.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (et_first_name.text.length>=2)
                    et_first_name.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_done, 0);
                else
                    et_first_name.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_close, 0);
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })
        et_last_name.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (et_last_name.text.length>=2)
                    et_last_name.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_done, 0);
                else
                    et_last_name.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_close, 0);
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })
    }
    fun hideKeyboardFrom(context: Context, view: View) {
        val imm = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

}
