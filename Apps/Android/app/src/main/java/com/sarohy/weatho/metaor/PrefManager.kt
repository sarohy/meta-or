package com.sarohy.weatho.metaor

import android.content.Context
import android.content.SharedPreferences

class PrefManager(context:Context) {
    var pref: SharedPreferences? = null
    var prefEditor: SharedPreferences.Editor? = null
    val SETTINGS_NAME = "default_settings"

    init {
        pref = context.getSharedPreferences(SETTINGS_NAME, Context.MODE_PRIVATE);
        prefEditor = pref?.edit();
    }

    fun saveDeviceID(deviceId:String){
        prefEditor?.putString("DeviceId",deviceId)
        prefEditor?.apply()
    }



    fun saveToken(token: String) {
        prefEditor?.putString("Token",token)
        prefEditor?.apply()
    }

    fun getEmail(): String {
        return pref?.getString("Email",Utils.UUID()) ?: Utils.UUID()
    }

    fun saveEmail(token: String) {
        prefEditor?.putString("Email",token)
        prefEditor?.apply()
    }

    fun getToken(): String {
        return pref?.getString("Token","") ?: ""
    }

    fun getAccNo(): String {
        return pref?.getString("AccNo","") ?: ""
    }

    fun getServerName(): String {
        return pref?.getString("ServerName","") ?: ""
    }

    fun setServerName(value: String) {
        prefEditor?.putString("ServerName",value)
        prefEditor?.apply()
    }

    fun setAccNo(value: String) {
        prefEditor?.putString("AccNo",value)
        prefEditor?.apply()
    }


    fun saveNotiData(value: String, checked: Boolean) {
        prefEditor?.putBoolean(value,checked)
        prefEditor?.apply()
    }

    fun getNotiData(value: String): Boolean {
        return pref?.getBoolean(value,true) ?: true
    }

    companion object {
        var prefManager:PrefManager? = null
        fun getInstance(context:Context):PrefManager {
            if (prefManager == null) {
                prefManager = PrefManager(context.applicationContext)
            }
            return prefManager as PrefManager
        }
    }
    fun getDeviceId(): String {
        val deviceId = pref?.getString("DeviceId",Utils.UUID()) ?: Utils.UUID()
        saveDeviceID(deviceId)
        return deviceId
    }
}