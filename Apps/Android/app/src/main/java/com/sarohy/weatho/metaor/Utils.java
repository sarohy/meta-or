package com.sarohy.weatho.metaor;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.Patterns;
import android.widget.TextView;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class Utils {
    public static String PRIVATE_KEY = "9463b813eb4a43d2b5246d6802894c7d";

    public static String MD5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }

    public static void showToast(@NotNull Context context, @NotNull String errorDesc) {
        Toast.makeText(context,errorDesc,Toast.LENGTH_SHORT).show();
    }

    public static void UpdateUIColor(Context context,String str, TextView view) {
        if (str.charAt(0)!='-') {
            view.setTextColor(Color.parseColor(context.getString(R.string.green)));
        }
        else {
            view.setTextColor(Color.parseColor(context.getString(R.string.red)));
        }
    }

    @NotNull
    public static String UUID() {
        return UUID.randomUUID().toString();
    }

    public static Boolean isValidEmail(CharSequence target){
        return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

}
