package com.sarohy.weatho.metaor


import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import org.json.JSONArray
import org.json.JSONObject
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_history.*
import android.support.v7.widget.DividerItemDecoration


class HistoryFragment : Fragment(), View.OnClickListener {

    lateinit var list: ArrayList<Trade>
    lateinit var mAdapter: TradeAdapter
    val handler = Handler()
    var c: Context? = context


    override fun onClick(view: View?) {
        view?.let {
            when(view.id){
                R.id.tv_today -> {
                    resetAllTabs()
                    period = "today"
                    loadData()
                    tv_today.setBackgroundResource(R.drawable.ic_box_filled)
                }
                R.id.tv_week -> {
                    resetAllTabs()
                    period = "week"
                    loadData()
                    tv_week.setBackgroundResource(R.drawable.ic_box_filled)
                }
                R.id.tv_month -> {
                    resetAllTabs()
                    period = "month"
                    loadData()
                    tv_month.setBackgroundResource(R.drawable.ic_box_filled)
                }
                R.id.tv_inception -> {
                    resetAllTabs()
                    period = "since_inception"
                    loadData()
                    tv_inception.setBackgroundResource(R.drawable.ic_box_filled)
                }
            }
        }
    }

    fun resetAllTabs() {
        tv_inception.setBackgroundResource(R.drawable.ic_box_border)
        tv_month.setBackgroundResource(R.drawable.ic_box_border)
        tv_week.setBackgroundResource(R.drawable.ic_box_border)
        tv_today.setBackgroundResource(R.drawable.ic_box_border)
        reset()
        c = null
        handler.removeCallbacksAndMessages(null)
        c = context
    }

    var period: String = "today"

    fun loadData() {
        context?.let {
            NetworkManager.getHistory(it, period, ProgressBar(it))
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_history, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setListerner()
    }

    fun init() {
        list = ArrayList()
        mAdapter = TradeAdapter(context!!,list)
        val mLayoutManager = LinearLayoutManager(context)
        rv_trade.layoutManager = mLayoutManager
        rv_trade.itemAnimator = DefaultItemAnimator()
        rv_trade.addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
        rv_trade.adapter = mAdapter
    }

    fun setListerner() {
        tv_today.setOnClickListener(this)
        tv_month.setOnClickListener(this)
        tv_week.setOnClickListener(this)
        tv_inception.setOnClickListener(this)
    }

    fun getDataWithDelay() {
        handler.postDelayed({
            loadData()
        }, 5000)
    }

    fun onSuccess(result: JSONObject?) {
        result?.let {
            list.clear()
            Log.d("Md5", result.toString())
            val response: JSONObject = result.get("result") as JSONObject
            updateTradeList(response)
            setTotalProfit(response)
            c?.let {
                getDataWithDelay()
            }
        }
    }

    fun updateTradeList(response: JSONObject) {
        val trades: JSONArray = response.get("trades") as JSONArray
        for (i in 0 until trades.length()) {
            val t = trades.getJSONObject(i)
            val trade = Trade(t.get("symbol") as String?, t.get("direction") as String?,
                    t.get("entry_price") as String?, t.get("size") as String?,
                    t.get("closePrice") as String?, t.get("profit") as String?)
            list.add(trade)
        }
        if(list.size>0) {
            mAdapter.notifyDataSetChanged()
        }
        else {
            rv_trade.visibility = View.GONE
            rl_empty.visibility = View.VISIBLE
        }
    }

    @SuppressLint("ResourceAsColor")
    fun setTotalProfit(response: JSONObject) {
        val totals: JSONObject = response.get("totals") as JSONObject
        tv_total_profit.text = totals.getString("sum_total_profit")
        Utils.UpdateUIColor(context,totals.getString("sum_total_profit"),tv_total_profit)
    }
    fun reset(){
        rv_trade.visibility = View.VISIBLE
        rl_empty.visibility = View.GONE
        handler.removeCallbacksAndMessages(null)
    }

    override fun onStop() {
        c = null
        handler.removeCallbacksAndMessages(null)
        super.onStop()
    }

    override fun onResume() {
        c = context
        loadData()
        super.onResume()
    }
}
