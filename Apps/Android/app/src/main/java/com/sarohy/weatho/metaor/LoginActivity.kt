package com.sarohy.weatho.metaor

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ProgressBar
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONObject
import android.content.Context.INPUT_METHOD_SERVICE
import android.view.inputmethod.InputMethodManager
import android.app.Activity
import android.net.Uri
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import android.widget.AutoCompleteTextView
import android.widget.ArrayAdapter
import org.json.JSONArray
import java.io.File
import android.support.design.widget.Snackbar




class LoginActivity : AppCompatActivity(), View.OnClickListener, CallBack {
    override fun onFail(result: JSONObject?) {
        result?.let { it ->
            loader.visibility = View.GONE
            btn_login.text = "Login"
            Snackbar.make(rootLayout, it.get("desc").toString(), Snackbar.LENGTH_INDEFINITE)
                    .setAction("Ok") {
                        btn_login.isClickable = true
                    }
                    .setActionTextColor(resources.getColor(android.R.color.holo_red_light))
                    .show()
        }
    }

    override fun onFetchSever(result: JSONObject?) {
        Severs.clear()
        val trades: JSONArray = result?.get("result") as JSONArray
        for (i in 0 until trades.length()) {
            val t = trades.getString(i)
            Severs.add(t.toString())
        }
        if (trades.length() > 0) {
            adapter.notifyDataSetChanged()
        }
    }

    override fun onSuccessHistory(result: JSONObject?) {

    }

    override fun onSuccessInfo(result: JSONObject?) {

    }

    override fun onSuccess(result: JSONObject?) {
        loader.visibility = View.GONE
        startActivity(Intent(this, MainActivity::class.java))
    }

    private val Severs = arrayListOf<String>()
    lateinit var adapter: ArrayAdapter<String>

    @SuppressLint("MissingPermission")
    override fun onCreate(savedInstanceState: Bundle?) {
        if (PrefManager.getInstance(this).getToken() != "") {
            finish()
            startActivity(Intent(this, MainActivity::class.java))
        } else {
            NetworkManager.getSever(this, ProgressBar(this))
        }

        this.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        btn_login.setOnClickListener(this)
        tv_signup.setOnClickListener(this)
        tv_click.setOnClickListener(this)
        adapter = ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, Severs)
        val textView = findViewById<AutoCompleteTextView>(R.id.et_server_name)
        textView.setAdapter<ArrayAdapter<String>>(adapter)
        et_server_name.setOnEditorActionListener() { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                btn_login.performClick()
                true
            } else {
                false
            }
        }
        et_email.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (Utils.isValidEmail(et_email.text))
                    et_email.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_done, 0);
                else
                    et_email.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_close, 0);
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })
        et_login_number.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (et_login_number.text.isNotEmpty())
                    et_login_number.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_done, 0);
                else
                    et_login_number.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_close, 0);
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })
        et_password.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (et_password.text.isNotEmpty())
                    et_password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_done, 0);
                else
                    et_password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_close, 0);
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })
        et_server_name.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (et_server_name.text.isNotEmpty())
                    et_server_name.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_done, 0);
                else
                    et_server_name.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_close, 0);
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_login -> {
                hideKeyboardFrom(this, v)
                if (Utils.isValidEmail(et_email.text) and (et_login_number.text.isNotEmpty()) and (et_password.text.isNotEmpty()) and (et_server_name.text.isNotEmpty())) {
                    this.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                    val email = et_email.text.toString();
                    val serverName = et_server_name.text.toString();
                    val password = et_password.text.toString();
                    val loginNumber = et_login_number.text.toString();
                    btn_login.text = "Loading"
                    loader.visibility = View.VISIBLE
                    btn_login.isClickable = false
                    NetworkManager.login(this, email, loginNumber, password, serverName, ProgressBar(this))
                } else
                    Utils.showToast(this, "Input is invalid")
            }
            R.id.tv_signup -> startActivity(Intent(this, SignUpActivity::class.java))
            R.id.tv_click -> startActivity(Intent(this, InfoActivity::class.java))
        }
    }

    fun hideKeyboardFrom(context: Context, view: View) {
        val imm = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

}
