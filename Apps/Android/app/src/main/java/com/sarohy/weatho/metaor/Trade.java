package com.sarohy.weatho.metaor;

public class Trade {
    String symbol;
    String direction;
    String entry_price;
    String size;
    String closePrice;
    String profit;

    public Trade(String symbol, String direction, String entry_price, String size, String closePrice, String profit) {
        this.symbol = symbol;
        this.direction = direction;
        this.entry_price = entry_price;
        this.size = size;
        this.closePrice = closePrice;
        this.profit = profit;
    }


    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getEntry_price() {
        return entry_price;
    }

    public void setEntry_price(String entry_price) {
        this.entry_price = entry_price;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getClosePrice() {
        return closePrice;
    }

    public void setClosePrice(String closePrice) {
        this.closePrice = closePrice;
    }

    public String getProfit() {
        return profit;
    }

    public void setProfit(String profit) {
        this.profit = profit;
    }
}
