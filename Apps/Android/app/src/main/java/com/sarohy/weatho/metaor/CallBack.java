package com.sarohy.weatho.metaor;

import org.json.JSONObject;

public interface CallBack {
    void onSuccess(JSONObject result);
    void onFail(JSONObject result);
    void onFetchSever(JSONObject result);
    void onSuccessInfo(JSONObject result);
    void onSuccessHistory(JSONObject result);
}
