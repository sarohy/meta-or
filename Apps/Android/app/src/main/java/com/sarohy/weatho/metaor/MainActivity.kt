package com.sarohy.weatho.metaor

import android.content.Context
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Handler
import android.view.WindowManager
import java.io.File
import android.net.DhcpInfo
import android.net.Uri
import android.net.wifi.WifiManager
import android.text.format.Formatter
import android.util.Log


class MainActivity : AppCompatActivity(), CallBack {

    val handler = Handler()

    override fun onFail(result: JSONObject?) {
        handler.removeCallbacksAndMessages(null)
        iv_online.setImageResource(R.drawable.ic_connecting)
        if (supportFragmentManager.fragments[0].tag == "Home"){
            val ft = supportFragmentManager.beginTransaction()
            ft.replace(R.id.fragment_container, HomeFragment(),"Home")
            ft.commit()
        }
        else {
            val ft = supportFragmentManager.beginTransaction()
            ft.replace(R.id.fragment_container, HomeFragment(),"Home")
            ft.commit()
        }
    }

    override fun onFetchSever(result: JSONObject?) {

    }

    override fun onSuccessInfo(result: JSONObject?) {
        updateStatus()
        if (supportFragmentManager.findFragmentByTag("Home")!= null) {
            val fragment: HomeFragment = supportFragmentManager.findFragmentByTag("Home") as HomeFragment
            fragment.onSuccess(result)
        }
    }

    override fun onSuccessHistory(result: JSONObject?) {
        updateStatus()
        if (supportFragmentManager.findFragmentByTag("History")!= null) {
            val fragment: HistoryFragment = supportFragmentManager.findFragmentByTag("History") as HistoryFragment
            fragment.onSuccess(result)
        }
    }

    override fun onSuccess(result: JSONObject?) {
        PrefManager.getInstance(this).saveToken("");
        finish()
        startActivity(Intent(this,LoginActivity::class.java))
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                val ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.fragment_container, HomeFragment(),"Home")
                ft.commit()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_dashboard -> {
                val ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.fragment_container, HistoryFragment(),"History")
                ft.commit()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_notifications -> {
                val ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.fragment_container, SettingFragment(),"Setting")
                ft.commit()
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val ft = supportFragmentManager.beginTransaction()
        ft.replace(R.id.fragment_container, HomeFragment(),"Home")
        ft.commit()
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        notificationUpdate()
        updateStatus()
    }

    private fun updateStatus() {
        handler.postDelayed({
            if (isNetworkAvailable())
                iv_online.setImageResource(R.drawable.ic_online)
            else
                iv_online.setImageResource(R.drawable.ic_no_internet)
            updateStatus()
        }, 2000)
    }

    fun notificationUpdate() {
        baseContext?.let {
            val stop_loss = PrefManager.getInstance(it).getNotiData("stop_loss")
            val loss = PrefManager.getInstance(it).getNotiData("loss")
            val take_profit = PrefManager.getInstance(it).getNotiData("take_profit")
            val profit = PrefManager.getInstance(it).getNotiData("profit")
            val open_pos = PrefManager.getInstance(it).getNotiData("open_pos")
            if (!stop_loss and !loss and !take_profit and !profit and !open_pos) {
                notification_bell.setImageResource(R.drawable.ic_notification_off)
            } else if (stop_loss and loss and take_profit and profit and open_pos) {
                notification_bell.setImageResource(R.drawable.ic_notifcation_on)
            } else {
                notification_bell.setImageResource(R.drawable.ic_notifcation_partial)
            }
        }
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }
}
