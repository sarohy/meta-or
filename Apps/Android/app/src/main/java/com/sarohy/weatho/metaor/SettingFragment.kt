package com.sarohy.weatho.metaor

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_setting.*
import java.lang.reflect.Array.getLength
import android.media.MediaPlayer
import android.content.res.AssetFileDescriptor
import android.widget.ProgressBar
import android.widget.Switch


class SettingFragment : Fragment(), View.OnClickListener {


    var player = MediaPlayer()

    override fun onClick(v: View?) {
        v?.let {
            when (it.id) {
                R.id.tv_logout -> {
                    NetworkManager.logout(context!!, ProgressBar(context!!))
                }
                R.id.play_stop_loss -> {playSound(R.raw.stop_loss)}
                R.id.play_loss -> {playSound(R.raw.loss)}
                R.id.play_take_profit -> {playSound(R.raw.take_profit)}
                R.id.play_profit -> {playSound(R.raw.profit)}
                R.id.play_open_pos -> {playSound(R.raw.open_pos)}

                R.id.stop_loss -> {saveNoti("stop_loss",stop_loss)}
                R.id.loss -> {saveNoti("loss",loss)}
                R.id.take_profit -> {saveNoti("take_profit",take_profit)}
                R.id.profit -> {saveNoti("profit",profit)}
                R.id.open_pos -> {saveNoti("open_pos",open_pos)}
                R.id.tv_link ->{
                    val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("http://www.metaor.org"))
                    startActivity(browserIntent)
                }
            }
        }
    }

    private fun saveNoti(value: String, switch: Switch) {
        context?.let {
            PrefManager.getInstance(it).saveNotiData(value,switch.isChecked)
        }
        (activity as MainActivity).notificationUpdate()
    }

    fun playSound(soundRaw:Int){
        context?.let {
            player.stop()
            player.release()
            player = MediaPlayer.create(it, soundRaw)
            player.start ()

        }
    }

    override fun onDestroy() {
        super.onDestroy()
        player.stop()
        player.release()
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_setting, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tv_logout.setOnClickListener(this)
        play_stop_loss.setOnClickListener(this)
        play_loss.setOnClickListener(this)
        play_take_profit.setOnClickListener(this)
        play_profit.setOnClickListener(this)
        play_open_pos.setOnClickListener(this)
        stop_loss.setOnClickListener(this)
        loss.setOnClickListener(this)
        take_profit.setOnClickListener(this)
        profit.setOnClickListener(this)
        open_pos.setOnClickListener(this)
        tv_link.setOnClickListener(this)
        context?.let {
            stop_loss.isChecked = PrefManager.getInstance(it).getNotiData("stop_loss")
            loss.isChecked = PrefManager.getInstance(it).getNotiData("loss")
            take_profit.isChecked = PrefManager.getInstance(it).getNotiData("take_profit")
            profit.isChecked = PrefManager.getInstance(it).getNotiData("profit")
            open_pos.isChecked = PrefManager.getInstance(it).getNotiData("open_pos")

        }
    }
}
