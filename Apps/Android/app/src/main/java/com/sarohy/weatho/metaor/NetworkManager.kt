package com.sarohy.weatho.metaor

import android.content.Context
import android.os.Build
import android.widget.ProgressBar
import com.android.volley.toolbox.Volley
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.JsonHttpResponseHandler
import com.loopj.android.http.RequestParams
import cz.msebera.android.httpclient.Header
import org.json.JSONObject
import java.util.*

class NetworkManager {
    companion object {
        val URL = "https://www.metaor.org:9750"
        fun signUp(context: Context, email: String, firstName: String, lastName: String, progressBar: ProgressBar) {
            var mRequestQueue = Volley.newRequestQueue(context);
            val client = AsyncHttpClient()
            client.setMaxRetriesAndTimeout(3, 30000);
            client.setUserAgent("android-async-http-1.4.9");
            val params: RequestParams = RequestParams()
            val action = "signup"
            val date = Date().time.toString()
            val deviceId = PrefManager.getInstance(context).getDeviceId();
            val deviceInfo = "Device info - " + Build.MANUFACTURER + "(" + Build.MODEL + ") - " + Build.VERSION.SDK
            params.put("action", action)
            params.put("timestamp", date)
            params.put("email", email)
            params.put("first_name", firstName)
            params.put("last_name", lastName)
            params.put("device_token", deviceId)
            params.put("device_info", deviceInfo)
            val concatenatedString = deviceInfo + deviceId + action + lastName + firstName + email + date + Utils.PRIVATE_KEY
            params.put("signature", Utils.MD5(concatenatedString))
            val header = arrayOf<Header>()
            client.post(context, URL,header,params,"application/x-www-form-urlencoded", object : JsonHttpResponseHandler() {

                override fun onStart() {
                    // called before request is started
                }

                override fun onSuccess(statusCode: Int, headers: Array<out Header>?, response: JSONObject?) {
                    super.onSuccess(statusCode, headers, response)
                    response?.let {
                        val code =  response.get("code")
                        val responsed:JSONObject = response.get("response") as JSONObject
                        if(code == 0){
                            PrefManager.getInstance(context).saveDeviceID(deviceId)
                            PrefManager.getInstance(context).saveEmail(email)
                            val callBack: CallBack = context as CallBack
                            callBack.onSuccess(responsed)
                        } else {
                            val callBack: CallBack = context as CallBack
                            callBack.onFail(responsed)

                        }
                    }
                }

                override fun onFailure(statusCode: Int, headers: Array<out Header>?, throwable: Throwable?, errorResponse: JSONObject?) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    Utils.showToast(context,"Network Issue")
                }

                override fun onRetry(retryNo: Int) {
                    // called when request is retried
                }
            })
        }

        fun getSever(context: Context, progressBar: ProgressBar) {
            var mRequestQueue = Volley.newRequestQueue(context);
            val client = AsyncHttpClient()
            client.setMaxRetriesAndTimeout(3, 30000);
            client.setUserAgent("android-async-http-1.4.9");
            val params: RequestParams = RequestParams()
            val action = "get_servers"
            val date = Date().time.toString()
            params.put("action", action)
            params.put("timestamp", date)
            val concatenatedString =action  + date + Utils.PRIVATE_KEY
            params.put("signature", Utils.MD5(concatenatedString))
            val header = arrayOf<Header>()
            client.post(context, URL,header,params,"application/x-www-form-urlencoded", object : JsonHttpResponseHandler() {

                override fun onStart() {
                    // called before request is started
                }

                override fun onSuccess(statusCode: Int, headers: Array<out Header>?, response: JSONObject?) {
                    super.onSuccess(statusCode, headers, response)
                    response?.let {
                        val code =  response.get("code")
                        val responsed:JSONObject = response.get("response") as JSONObject
                        if(code == 0){
                            val callBack: CallBack = context as CallBack
                            callBack.onFetchSever(responsed)
                            Utils.showToast(context,responsed.get("msg").toString())
                        }
                        else {
                            Utils.showToast(context,responsed.get("msg").toString())
                        }
                    }
                }

                override fun onFailure(statusCode: Int, headers: Array<out Header>?, throwable: Throwable?, errorResponse: JSONObject?) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    Utils.showToast(context,"Network Issue")
                }

                override fun onRetry(retryNo: Int) {
                    // called when request is retried
                }
            })
        }


        fun login(context: Context, email: String, loginNumber: String, password: String, serverName: String, progressBar: ProgressBar) {
            val client = AsyncHttpClient()
            client.setMaxRetriesAndTimeout(3, 30000);
            client.setUserAgent("android-async-http-1.4.9");
            val params: RequestParams = RequestParams()
            val action = "login"
            val date = Date().time.toString()
            val deviceId = PrefManager.getInstance(context).getDeviceId();
            val deviceInfo = "Device info - " + Build.MANUFACTURER + "(" + Build.MODEL + ") - " + Build.VERSION.SDK
            params.put("action", action)
            params.put("timestamp", date)
            params.put("email", email)
            params.put("mt4_account", loginNumber)
            params.put("mt4_password", password)
            params.put("mt4_server", serverName)
            params.put("device_token", deviceId)
            params.put("device_info", deviceInfo)
            val concatenatedString = password + serverName + deviceInfo + deviceId + action + loginNumber + email + date + Utils.PRIVATE_KEY
            params.put("signature", Utils.MD5(concatenatedString))
            val header = arrayOf<Header>()
            client.post(context, URL, header, params, "application/x-www-form-urlencoded", object : JsonHttpResponseHandler() {

                override fun onStart() {
                    // called before request is started
                }

                override fun onSuccess(statusCode: Int, headers: Array<out Header>?, response: JSONObject?) {
                    super.onSuccess(statusCode, headers, response)
                    response?.let {
                        var code = response.get("code")
                        var responsed: JSONObject = response.get("response") as JSONObject
                        if (code == 0) {
                            PrefManager.getInstance(context).saveToken(responsed.get("mt4_account_token").toString())
                            PrefManager.getInstance(context).setAccNo(loginNumber)
                            PrefManager.getInstance(context).setServerName(serverName)

                            Utils.showToast(context, responsed.get("desc").toString())
                            val callBack: CallBack = context as CallBack
                            callBack.onSuccess(responsed)
                        } else {
                            val callBack: CallBack = context as CallBack
                            callBack.onFail(responsed)

                        }
                    }
                }

                override fun onFailure(statusCode: Int, headers: Array<out Header>?, throwable: Throwable?, errorResponse: JSONObject?) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    Utils.showToast(context, "Network Issue")
                }

                override fun onRetry(retryNo: Int) {
                    // called when request is retried
                }
            })
        }

        fun logout(context: Context, progressBar: ProgressBar) {
            val client = AsyncHttpClient()
            client.setMaxRetriesAndTimeout(3, 30000);
            client.setUserAgent("android-async-http-1.4.9");
            val params: RequestParams = RequestParams()
            val action = "logout"
            val date = Date().time.toString()
            val token = PrefManager.getInstance(context).getToken()
            val email = PrefManager.getInstance(context).getEmail()
            val deviceId = PrefManager.getInstance(context).getDeviceId();
            params.put("action", action)
            params.put("timestamp", date)
            params.put("device_token", deviceId)
            params.put("mt4_account_token", token)
            params.put("email", email)
            val concatenatedString =  deviceId + action + token + email+ date + Utils.PRIVATE_KEY
            params.put("signature", Utils.MD5(concatenatedString))
            val header = arrayOf<Header>()
            client.post(context, URL, header, params, "application/x-www-form-urlencoded", object : JsonHttpResponseHandler() {

                override fun onStart() {
                    // called before request is started
                }

                override fun onSuccess(statusCode: Int, headers: Array<out Header>?, response: JSONObject?) {
                    super.onSuccess(statusCode, headers, response)
                    response?.let {
                        var code = response.get("code")
                        var responsed: JSONObject = response.get("response") as JSONObject
                        if (code == 0) {
                            val callBack: CallBack = context as CallBack
                            callBack.onSuccess(responsed)
                        } else {
                            val callBack: CallBack = context as CallBack
                            callBack.onFail(responsed)

                        }
                    }
                }

                override fun onFailure(statusCode: Int, headers: Array<out Header>?, throwable: Throwable?, errorResponse: JSONObject?) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    Utils.showToast(context, "Network Issue")
                }

                override fun onRetry(retryNo: Int) {
                    // called when request is retried
                }
            })
        }

        fun getInfo(context: Context, progressBar: ProgressBar) {
            val client = AsyncHttpClient()
            client.setMaxRetriesAndTimeout(3, 30000);
            client.setUserAgent("android-async-http-1.4.9");
            val params: RequestParams = RequestParams()
            val action = "get_info"
            val date = Date().time.toString()
            val deviceId = PrefManager.getInstance(context).getDeviceId()
            val email = PrefManager.getInstance(context).getEmail()
            val token = PrefManager.getInstance(context).getToken()
            params.put("action", action)
            params.put("email", email)
            params.put("mt4_account_token", token)
            params.put("device_token", deviceId)
            params.put("timestamp", date)
            val concatenatedString = deviceId + action + email + token + date + Utils.PRIVATE_KEY
            params.put("signature", Utils.MD5(concatenatedString))
            val header = arrayOf<Header>()
            client.post(context, URL, header, params, "application/x-www-form-urlencoded", object : JsonHttpResponseHandler() {

                override fun onStart() {
                    // called before request is started
                }

                override fun onSuccess(statusCode: Int, headers: Array<out Header>?, response: JSONObject?) {
                    super.onSuccess(statusCode, headers, response)
                    response?.let {
                        var code = response.get("code")
                        var responsed: JSONObject = response.get("response") as JSONObject
                        if (code == 0) {
                            val callBack: CallBack = context as CallBack
                            callBack.onSuccessInfo(responsed)
                        } else {
                            Utils.showToast(context, responsed.get("desc").toString())
                        }
                    }
                }

                override fun onFailure(statusCode: Int, headers: Array<out Header>?, throwable: Throwable?, errorResponse: JSONObject?) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    val callBack: CallBack = context as CallBack
                    callBack.onFail(errorResponse)
                }

                override fun onRetry(retryNo: Int) {
                    var h = 1
                    // called when request is retried
                }
            })
        }

        fun getHistory(context: Context, period: String, progressBar: ProgressBar) {
            val client = AsyncHttpClient()
            client.setMaxRetriesAndTimeout(3, 30000);
            client.setUserAgent("android-async-http-1.4.9");
            val params: RequestParams = RequestParams()
            val action = "get_history"
            val date = Date().time.toString()
            val deviceId = PrefManager.getInstance(context).getDeviceId()
            val email = PrefManager.getInstance(context).getEmail()
            val token = PrefManager.getInstance(context).getToken()
            params.put("action", action)
            params.put("email", email)
            params.put("mt4_account_token", token)
            params.put("device_token", deviceId)
            params.put("timestamp", date)
            params.put("history_period",period)
            val concatenatedString = period + deviceId + action + email + token + date + Utils.PRIVATE_KEY
            params.put("signature", Utils.MD5(concatenatedString))
            val header = arrayOf<Header>()
            client.post(context, URL, header, params, "application/x-www-form-urlencoded", object : JsonHttpResponseHandler() {

                override fun onStart() {
                    // called before request is started
                }

                override fun onSuccess(statusCode: Int, headers: Array<out Header>?, response: JSONObject?) {
                    super.onSuccess(statusCode, headers, response)
                    response?.let {
                        var code = response.get("code")
                        var responsed: JSONObject = response.get("response") as JSONObject
                        if (code == 0) {
                            val callBack: CallBack = context as CallBack
                            callBack.onSuccessHistory(responsed)
                        } else {
                            Utils.showToast(context, responsed.get("desc").toString())
                        }
                    }
                }

                override fun onFailure(statusCode: Int, headers: Array<out Header>?, throwable: Throwable?, errorResponse: JSONObject?) {
                    super.onFailure(statusCode, headers, throwable, errorResponse)
                    Utils.showToast(context, "Network Issue")
                }

                override fun onRetry(retryNo: Int) {
                    // called when request is retried
                }
            })
        }
    }
}